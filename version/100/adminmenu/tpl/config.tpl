<div class="control-group config">\
    <a class="remove-group" href="#"><i class="fa fa-remove"></i></a>\
    <label class="control-label" for="">Name</label>\
    <div class="controls">\
        <input{if !empty($setting->Name)} value="{$setting->Name}" {/if} type="text" name="jtlshop3plugin-Settingslink-Setting-Name[]" class="form-control required" placeholder="Option Nummer X">\
    </div>\
    <label class="control-label" for="">Einstellungs-Name</label>\
    <div class="controls">\
        <input{if !empty($setting->ValueName)} value="{$setting->ValueName}" {/if} type="text" name="jtlshop3plugin-Settingslink-Setting-ValueName[]" class="form-control required" placeholder="my_option_x">\
    </div>\
    <label class="control-label" for="">Beschreibung</label>\
    <div class="controls">\
        <input{if !empty($setting->Description)} value="{$setting->Description}" {/if} type="text" name="jtlshop3plugin-Settingslink-Setting-Description[]" class="form-control required" placeholder="Hiermit stellt man etwas ein">\
    </div>\
    <label class="control-label" for="">Anfangswert</label>\
    <div class="controls">\
        <input{if !empty($setting->initialValue)} value="{$setting->initialValue}" {/if} type="text" name="jtlshop3plugin-Settingslink-Setting-initialValue[]" class="form-control required" placeholder="42">\
    </div>\
    <label class="control-label" for="">Typ</label>\
    <div class="controls">\
        <select class="config-dropdown form-control" name="jtlshop3plugin-Settingslink-Setting-type[]">\
            <option{if !empty($setting->type) && $setting->type === 'text'} selected{/if} value="text">Text</option>\
            <option{if !empty($setting->type) && $setting->type === 'selectbox'} selected{/if} value="selectbox">Selectbox</option>\
            <option{if !empty($setting->type) && $setting->textarea === 'text'} selected{/if} value="textarea">Textarea</option>\
            <option{if !empty($setting->type) && $setting->checkbox === 'text'} selected{/if} value="checkbox">Checkbox</option>\
            <option{if !empty($setting->type) && $setting->radio === 'text'} selected{/if} value="radio">Radio</option>\
        </select>\
    </div>\
    <div class="option-values-wrap" style="display:none;">\
        <hr>\
        <button class="add-option-values btn btn-primary" type="button">Wert hinzuf&uuml;gen</button>\
        <div class="option-values"></div>\
        <hr>\
    </div>\
    <hr>\
</div>\
