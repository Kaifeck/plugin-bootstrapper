<div class="control-group">\
    <a class="remove-group" href="#"><i class="fa fa-remove"></i></a>\
    <label class="control-label" for="">Name</label>\
    <div class="controls">\
        <input{if !empty($box->Name)} value="{$box->Name}" {/if} type="text" name="jtlshop3plugin-Install-Boxes-Box-Name[]" class="form-control required" placeholder="Meine tolle Box">\
    </div>\
    <label class="control-label" for="">Template-Datei</label>\
    <div class="controls">\
        <input{if !empty($box->TemplateFile)} value="{$box->TemplateFile}" {/if} type="text" name="jtlshop3plugin-Install-Boxes-Box-TemplateFile[]" class="form-control required" placeholder="my_box.tpl">\
    </div>\
    <label class="control-label" for="">Aktiviert auf Seitentyp (0 f&uuml;r alle)</label>\
    <div class="controls">\
        <input{if isset($box->Available)} value="{$box->Available}" {/if} type="text" name="jtlshop3plugin-Install-Boxes-Box-Available[]" class="form-control required" placeholder="0">\
    </div>\
    <hr>\
</div>\
