<div class="control-group">\
    <a class="remove-group" href="#"><i class="fa fa-remove"></i></a>\
    <label class="control-label" for="">Dateiame</label>\
    <div class="controls">\
        <input{if !empty($jsFile->name)} value="{$jsFile->name}" {/if} type="text" name="jtlshop3plugin-Install-JS-file-name[]" class="form-control required" placeholder="meine.datei.js">\
    </div>\
    <label class="control-label" for="">Priorit&auml;t</label>\
    <div class="controls">\
        <select name="jtlshop3plugin-Install-JS-file-priority[]" class="form-control required">\
            <option{if !empty($jsFile->priority) && $jsFile->priority == 1} selected {/if} value="1">1 (h&ouml;chste)</option>\
            <option{if !empty($jsFile->priority) && $jsFile->priority == 2} selected {/if} value="2">2</option>\
            <option{if !empty($jsFile->priority) && $jsFile->priority == 3} selected {/if} value="3">3</option>\
            <option{if !empty($jsFile->priority) && $jsFile->priority == 4} selected {/if} value="4">4</option>\
            <option{if empty($jsFile->priority) || $jsFile->priority == 5} selected {/if} value="5">5 (Standard)</option>\
            <option{if !empty($jsFile->priority) && $jsFile->priority == 6} selected {/if} value="6">6</option>\
            <option{if !empty($jsFile->priority) && $jsFile->priority == 7} selected {/if} value="7">7</option>\
            <option{if !empty($jsFile->priority) && $jsFile->priority == 9} selected {/if} value="8">8</option>\
            <option{if !empty($jsFile->priority) && $jsFile->priority == 9} selected {/if} value="9">9</option>\
            <option{if !empty($jsFile->priority) && $jsFile->priority == 10} selected {/if} value="10">10 (niedrigste)</option>\
        </select>\
    </div>\
    <label class="control-label" for="">Position</label>\
    <div class="controls">\
        <select name="jtlshop3plugin-Install-JS-file-position[]" class="form-control required">\
            <option{if !empty($jsFile->position) && $jsFile->position === 'body'} selected {/if} value="body">Body</option>\
            <option{if empty($jsFile->position) || $jsFile->position === 'head'} selected {/if} value="head">Head</option>\
        </select>\
    </div>\
    <hr>\
</div>\
