<style>
    .option-values-wrap{ldelim}padding-left:20px;{rdelim}
    #finish-root{ldelim}padding-top:20px;{rdelim}
</style>

{if !$permissionOK}
    <div class="alert alert-danger"><i class="fa fa-warning"></i> Keine Schreibrechte in includes/plugins vorhanden!</div>
{else}
    <section id="load-plugin">
        <div class="page-header">
            <h3>Bestehendes Plugin laden</h3>
        </div>
        <form id="loadPluginForm" method="post" action="" class="form-horizontal">
            {$jtl_token}
            <div class="input-group">
                <div class="input-group-addon">
                    <label for="path">Pfad (relativ zu includes/plugins)</label>
                </div>
                <input type="text" name="path" id="path" class="form-control">
                <div class="input-group-btn">
                    <button type="submit" name="loadPlugin" value="1" class="btn btn-default"><i class="fa fa-share"></i> Plugin laden</button>
                </div>
            </div>
       </form>
    </section>
    <section id="wizard">
        <div class="page-header">
            <h3>Neues Plugin erstellen</h3>
        </div>
        <form id="bootstrapForm" method="post" action="" class="form-horizontal">
            <input type="hidden" name="createPlugin" value="1">
            {$jtl_token}
            <div id="wizardRoot">
                <ul>
                    <li><a href="#tab1" data-toggle="tab">Allgemein</a></li>
                    <li><a href="#tab2" data-toggle="tab">Hooks</a></li>
                    <li><a href="#tab3" data-toggle="tab">Sprachvariablen</a></li>
                    <li><a href="#tab4" data-toggle="tab">Einstellungen</a></li>
                    <li><a href="#tab5" data-toggle="tab">Frontendlinks</a></li>
                    <li><a href="#tab6" data-toggle="tab">Backendlinks</a></li>
                    <li><a href="#tab7" data-toggle="tab">JavaScript</a></li>
                    <li><a href="#tab8" data-toggle="tab">CSS</a></li>
                    <li><a href="#tab9" data-toggle="tab">Boxen</a></li>
                    <li><a href="#tab10" data-toggle="tab">Email-Templates</a></li>
                    <li><a href="#tab11" data-toggle="tab">Abschlie&szlig;en</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane" id="tab1">
                        <div class="control-group">
                            <label class="control-label" for="jtlshop3plugin-Name">Plugin-Name</label>
                            <div class="controls">
                                <input{if !empty($loadedPlugin->Name)} value="{$loadedPlugin->Name|utf8_decode}"{/if} type="text" id="jtlshop3plugin-Name" name="jtlshop3plugin-Name" class="form-control required" placeholder="Mein tolles Plugin">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="jtlshop3plugin-PluginID">Plugin-ID</label>
                            <div class="controls">
                                <input{if !empty($loadedPlugin->PluginID)} value="{$loadedPlugin->PluginID}"{/if} type="text" id="jtlshop3plugin-PluginID" name="jtlshop3plugin-PluginID" class="form-control required" placeholder="mycompany_my_plugin">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="jtlshop3plugin-Description">Plugin-Beschreibung</label>
                            <div class="controls">
                                <input{if !empty($loadedPlugin->Description)} value="{$loadedPlugin->Description|utf8_decode}"{/if} type="text" id="jtlshop3plugin-Description" name="jtlshop3plugin-Description" class="form-control required" placeholder="Dieses Plugin macht gar nichts">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="jtlshop3plugin-Author">Plugin-Autor</label>
                            <div class="controls">
                                <input{if !empty($loadedPlugin->Author)} value="{$loadedPlugin->Author}"{/if} type="text" id="jtlshop3plugin-Author" name="jtlshop3plugin-Author" class="form-control required" placeholder="Manfred Mustermann">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="jtlshop3plugin-URL">Plugin-Homepage</label>
                            <div class="controls">
                                <input{if !empty($loadedPlugin->URL)} value="{$loadedPlugin->URL}"{/if} type="text" id="jtlshop3plugin-URL" name="jtlshop3plugin-URL" class="form-control required" placeholder="https://www.example.org/">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="jtlshop3plugin-Icon">Icon (muss im Hauptverzeichnis platziert werden)</label>
                            <div class="controls">
                                <input{if !empty($loadedPlugin->Icon)} value="{$loadedPlugin->Icon}"{/if} type="text" id="jtlshop3plugin-Icon" name="jtlshop3plugin-Icon" class="form-control" placeholder="mein-logo.png">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="jtlshop3plugin-FlushTags">Cache-Tags bei Installation l&ouml;schen (ab Shop 4.05)</label>
                            <div class="controls">
                                <input{if !empty($loadedPlugin->FlushTags)} value="{$loadedPlugin->FlushTags}"{/if} type="text" id="jtlshop3plugin-FlushTags" name="jtlshop3plugin-FlushTags" class="form-control" placeholder="CACHING_GROUP_CATEGORY, CACHING_GROUP_ARTICLE">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="jtlshop3plugin-Install-Version-SQL">Eigenes Installations-SQL?</label>
                            <div class="controls">
                                <input{if !empty($loadedPlugin->SQL)} checked{/if} type="checkbox" name="jtlshop3plugin-Install-Version-SQL" id="jtlshop3plugin-Install-Version-SQL">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="supportsShop3">Unterst&uuml;tzt JTL-Shop 3?</label>
                            <div class="controls">
                                <input{if !empty($loadedPlugin->ShopVersion)} checked{/if} type="checkbox" name="supportsShop3" id="supportsShop3" onclick="$('#shop3support').toggle();">
                            </div>
                        </div>
                        <div class="control-group" style="display:{if !empty($loadedPlugin->ShopVersion)}block{else}none{/if};" id="shop3support">
                            <label class="control-label" for="jtlshop3plugin-ShopVersion">Mindestversion JTL-Shop 3</label>
                            <div class="controls">
                                <input{if !empty($loadedPlugin->ShopVersion)} value="{$loadedPlugin->ShopVersion}" {/if} type="text" id="jtlshop3plugin-ShopVersion" name="jtlshop3plugin-ShopVersion" class="form-control required" placeholder="318">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="supportsShop4">Unterst&uuml;tzt JTL-Shop 4?</label>
                            <div class="controls">
                                <input{if !empty($loadedPlugin->Shop4Version)} checked{/if} type="checkbox" name="supportsShop4" id="supportsShop4" onclick="$('#shop4support').toggle();">
                            </div>
                        </div>
                        <div class="control-group" style="display:{if !empty($loadedPlugin->Shop4Version)}block{else}none{/if};" id="shop4support">
                            <label class="control-label" for="jtlshop3plugin-Shop4Version">Mindestversion JTL-Shop 4</label>
                            <div class="controls">
                                <input{if !empty($loadedPlugin->Shop4Version)} value="{$loadedPlugin->Shop4Version}" {/if} type="text" id="jtlshop3plugin-Shop4Version" name="jtlshop3plugin-Shop4Version" class="form-control required" placeholder="400">
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab2">
                        <button class="btn btn-primary" type="button" id="add-hook">Hook hinzuf&uuml;gen</button>
                        <div id="hook-selector-root">
                            {if !empty($loadedPlugin->Hooks)}
                                {foreach $loadedPlugin->Hooks as $hook}
                                    {include file=$tplPath|cat:'hookSelector.tpl' hook=$hook assign='html'}
                                    {$html|replace:"\\\n":''}
                                {/foreach}
                                {assign var=hook value=null}
                            {/if}
                        </div>
                    </div>
                    <div class="tab-pane" id="tab3">
                        <button class="btn btn-primary" type="button" id="add-lang-var">Sprachvariable hinzuf&uuml;gen</button>
                        <div id="lang-var-root">
                            {if !empty($loadedPlugin->LangVars)}
                                {foreach $loadedPlugin->LangVars as $langVar}
                                    {include file=$tplPath|cat:'langVar.tpl' langVar=$langVar assign='html'}
                                    {$html|replace:"\\\n":''}
                                {/foreach}
                                {assign var=langVar value=null}
                            {/if}
                        </div>
                    </div>
                    <div class="tab-pane" id="tab4">
                        <button class="btn btn-primary" type="button" id="add-config">Einstellung hinzuf&uuml;gen</button>
                        <div id="config-root">
                            {if !empty($loadedPlugin->Settings[0]->Settings)}
                                {foreach $loadedPlugin->Settings[0]->Settings as $setting}
                                    {include file=$tplPath|cat:'config.tpl' setting=$setting assign='html'}
                                    {$html|replace:"\\\n":''}
                                {/foreach}
                                {assign var=setting value=null}
                            {/if}
                        </div>
                    </div>
                    <div class="tab-pane" id="tab5">
                        <button class="btn btn-primary" type="button" id="add-frontend-link">Frontendlink hinzuf&uuml;gen</button>
                        <div id="frontend-link-root">
                            {if !empty($loadedPlugin->Frontendlinks)}
                                {foreach $loadedPlugin->Frontendlinks as $frontendLink}
                                    {include file=$tplPath|cat:'frontendLink.tpl' frontendLink=$frontendLink assign='html'}
                                    {$html|replace:"\\\n":''}
                                {/foreach}
                                {assign var=frontendLink value=null}
                            {/if}
                        </div>
                    </div>
                    <div class="tab-pane" id="tab6">
                        <button class="btn btn-primary" type="button" id="add-backend-link">Backendlink hinzuf&uuml;gen</button>
                        <div id="backend-link-root">
                            {if !empty($loadedPlugin->CustomLinks)}
                                {foreach $loadedPlugin->CustomLinks as $customLink}
                                    {include file=$tplPath|cat:'backendLink.tpl' customLink=$customLink assign='html'}
                                    {$html|replace:"\\\n":''}
                                {/foreach}
                                {assign var=customLink value=null}
                            {/if}
                        </div>
                    </div>
                    <div class="tab-pane" id="tab7">
                        <button class="btn btn-primary" type="button" id="add-javascript">JavaScript hinzuf&uuml;gen</button>
                        <div id="javascript-root">
                            {if !empty($loadedPlugin->Install->JS->file)}
                                {foreach $loadedPlugin->Install->JS->file as $jsFile}
                                    {include file=$tplPath|cat:'javascript.tpl' jsFile=$jsFile assign='html'}
                                    {$html|replace:"\\\n":''}
                                {/foreach}
                                {assign var=jsFile value=null}
                            {/if}
                        </div>
                    </div>
                    <div class="tab-pane" id="tab8">
                        <button class="btn btn-primary" type="button" id="add-css">CSS hinzuf&uuml;gen</button>
                        <div id="css-root">
                            {if !empty($loadedPlugin->Install->CSS->file)}
                                {foreach $loadedPlugin->Install->CSS->file as $cssFile}
                                    {include file=$tplPath|cat:'css.tpl' cssFile=$cssFile assign='html'}
                                    {$html|replace:"\\\n":''}
                                {/foreach}
                                {assign var=cssFile value=null}
                            {/if}
                        </div>
                    </div>
                    <div class="tab-pane" id="tab9">
                        <button class="btn btn-primary" type="button" id="add-box">Box hinzuf&uuml;gen</button>
                        <div id="box-root">
                            {if !empty($loadedPlugin->Boxes)}
                                {foreach $loadedPlugin->Boxes as $box}
                                    {include file=$tplPath|cat:'box.tpl' box=$box assign='html'}
                                    {$html|replace:"\\\n":''}
                                {/foreach}
                                {assign var=box value=null}
                            {/if}
                        </div>
                    </div>
                    <div class="tab-pane" id="tab10">
                        <button class="btn btn-primary" type="button" id="add-mail-template">Email-Template hinzuf&uuml;gen</button>
                        <div id="mail-template-root">
                            {if !empty($loadedPlugin->Emailtemplates)}
                                {foreach $loadedPlugin->Emailtemplates as $tpl}
                                    {include file=$tplPath|cat:'mailTemplate.tpl' tpl=$tpl assign='html'}
                                    {$html|replace:"\\\n":''}
                                {/foreach}
                                {assign var=tpl value=null}
                            {/if}
                        </div>
                    </div>
                    <div class="tab-pane" id="tab11">
                        <div id="finish-root"></div>
                    </div>
                    <ul class="pager wizard">
                        <li class="previous"><a href="#"><i class="fa fa-angle-left"></i> zur&uuml;ck</a></li>
                        <li class="next"><a href="#">weiter <i class="fa fa-angle-right"></i></a></li>
                        <li class="next finish" style="display:none;"><a href="#" id="finish-bootstrap"><i class="fa fa-share"></i> Plugin erstellen</a></li>
                    </ul>
                </div>
            </div>
        </form>
    </section>
    <script>
        $(document).ready(function() {ldelim}
            var optionCounter = 0;
            $('#wizardRoot').bootstrapWizard({ldelim}
                onTabShow: function(tab, navigation, index) {ldelim}
                    var total = navigation.find('li').length,
                        current = index + 1,
                        root = $('#wizardRoot');
                    if (current >= total) {ldelim}
                        root.find('.pager .next').hide();
                        root.find('.pager .finish').show();
                        root.find('.pager .finish').removeClass('disabled');
                    {rdelim} else {ldelim}
                        root.find('.pager .next').show();
                        root.find('.pager .finish').hide();
                    {rdelim}
                {rdelim},
                onNext: function(tab, navigation, index) {ldelim}
                    var cancel = false,
                        focusElement = null;
                    $('#tab' + index + ' input.required:visible').each(function (idx, elem) {ldelim}
                        if (elem.value === '') {ldelim}
                            focusElement = $(elem);
                            cancel = true;
                        {rdelim}
                    {rdelim});
                    if (cancel) {ldelim}
                        alert('Bitte alle Felder ausf�llen.');
                        focusElement.focus();
                        return false;
                    {rdelim}
                {rdelim}
            {rdelim});
            $('#add-hook').click(function () {ldelim}
                $('#hook-selector-root').append('{include file=$oPlugin->cAdminmenuPfad|cat:'tpl/hookSelector.tpl'}');
            {rdelim});
            $('#add-lang-var').click(function () {ldelim}
                $('#lang-var-root').append('{include file=$oPlugin->cAdminmenuPfad|cat:'tpl/langVar.tpl'}');
            {rdelim});
            $('#add-config').click(function () {ldelim}
                $('#config-root').append('{include file=$oPlugin->cAdminmenuPfad|cat:'tpl/config.tpl'}');
            {rdelim});
            $('#add-frontend-link').click(function () {ldelim}
                $('#frontend-link-root').append('{include file=$oPlugin->cAdminmenuPfad|cat:'tpl/frontendLink.tpl'}');
            {rdelim});
            $('#add-backend-link').click(function () {ldelim}
                $('#backend-link-root').append('{include file=$oPlugin->cAdminmenuPfad|cat:'tpl/backendLink.tpl'}');
            {rdelim});
            $('#add-javascript').click(function () {ldelim}
                $('#javascript-root').append('{include file=$oPlugin->cAdminmenuPfad|cat:'tpl/javascript.tpl'}');
            {rdelim});
            $('#add-css').click(function () {ldelim}
                $('#css-root').append('{include file=$oPlugin->cAdminmenuPfad|cat:'tpl/css.tpl'}');
            {rdelim});
            $('#add-box').click(function () {ldelim}
                $('#box-root').append('{include file=$oPlugin->cAdminmenuPfad|cat:'tpl/box.tpl'}');
            {rdelim});
            $('#add-mail-template').click(function () {ldelim}
                $('#mail-template-root').append('{include file=$oPlugin->cAdminmenuPfad|cat:'tpl/mailTemplate.tpl'}');
            {rdelim});
            $('#tab4').on('change', '.config-dropdown', function () {ldelim}
                var elem = $(this),
                    val = elem.val();
                if (val === 'selectbox' || val === 'radio') {ldelim}
                    optionCounter++;
                    elem.parent().parent().find('.option-values-wrap').show();
                {rdelim} else {ldelim}
                    optionCounter--;
                    elem.parent().parent().find('.option-values-wrap .option-values').html('');
                {rdelim}
            {rdelim}).on('click', '.add-option-values', function () {ldelim}
                var tpl = '{include file=$oPlugin->cAdminmenuPfad|cat:'tpl/selectRadioOptions.tpl'}';
                $(this).parent().find('.option-values').append(tpl.split('#counter#').join(optionCounter.toString()));
            {rdelim});
            $('.tab-pane').on('click', '.remove-group', function () {ldelim}
                $(this).parent('.control-group').remove();
                return false;
            {rdelim});

            $('#finish-bootstrap').click(function () {ldelim}
                $.ajax('{$adminURL}', {ldelim}
                    data: $('#bootstrapForm').serialize(),
                    type: 'POST',
                    success: function(response){ldelim}
                        var html = '',
                            i,
                            tplSuccess = '{include file=$oPlugin->cAdminmenuPfad|cat:'tpl/success.tpl'}',
                            tplError = '{include file=$oPlugin->cAdminmenuPfad|cat:'tpl/error.tpl'}';
                        if (typeof response.success !== 'undefined' && typeof response.status !== 'undefined') {ldelim}
                            if (response.success === true) {ldelim}
                                html = tplSuccess.replace('#PLUGIN#', response.status.plugin).replace('#PLUGINDIR#', response.status.plugindir);
                            {rdelim} else {ldelim}
                                for (i = 0; i < response.status.errors.length; i++) {ldelim}
                                    html += tplError.replace('#ERROR#', response.status.errors[i]);
                                {rdelim}
                            {rdelim}
                        {rdelim} else {ldelim}
                            html = tplError.replace('#ERROR', 'Keine Antwort via Ajax erhalten.');
                        {rdelim}
                        $('#finish-root').html(html);
                    {rdelim}
                {rdelim});
            {rdelim});
        {rdelim});
    </script>
{/if}