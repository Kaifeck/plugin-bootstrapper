<div class="control-group">\
    <a class="remove-group" href="#"><i class="fa fa-remove"></i></a>\
    <label class="control-label" for="hookIDs">Hook</label>\
    <div class="controls">\
        <select name="jtlshop3plugin-Install-Hooks-Hook-ID[]" class="form-control">\
            {strip}
            {foreach $hookList as $hookData}
                <option{if !empty($hook->id) && $hook->id == $hookData.2} selected{/if} value="{$hookData.2}">{$hookData.1} ({$hookData.2})</option>\
            {/foreach}
            {/strip}
        </select>\
        <label class="control-label" for="hookFile">Dateiname</label>\
        <div class="controls">\
            <input{if !empty($hook->file)} value="{$hook->file}"{/if} type="text" name="jtlshop3plugin-Install-Hooks-Hook[]" class="form-control required" placeholder="meine_hook_datei.php">\
        </div>\
    </div>\
    <hr>\
</div>\
