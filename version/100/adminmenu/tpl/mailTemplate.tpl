<div class="control-group">\
    <a class="remove-group" href="#"><i class="fa fa-remove"></i></a>\
    <label class="control-label" for="">Name</label>\
    <div class="controls">\
        <input{if !empty($tpl->Name)} value="{$tpl->Name}" {/if} type="text" name="jtlshop3plugin-Install-Emailtemplate-Template-Name[]" class="form-control required" placeholder="MeinMailTemplate">\
    </div>\
    <label class="control-label" for="">Beschreibung</label>\
    <div class="controls">\
        <input{if !empty($tpl->Description)} value="{$tpl->Description}" {/if} type="text" name="jtlshop3plugin-Install-Emailtemplate-Template-Description[]" class="form-control required" placeholder="Beschreibung f&uuml;r mein Template">\
    </div>\
    <label class="control-label" for="">Typ</label>\
    <div class="controls">\
        <input{if !empty($tpl->Type)} value="{$tpl->Type}" {/if} type="text" name="jtlshop3plugin-Install-Emailtemplate-Template-Type[]" class="form-control required" placeholder="text/html">\
    </div>\
    <label class="control-label" for="">Modul-ID (keine Unterstriche)</label>\
    <div class="controls">\
        <input{if !empty($tpl->ModulId)} value="{$tpl->ModulId}" {/if} type="text" name="jtlshop3plugin-Install-Emailtemplate-Template-ModulId[]" class="form-control required">\
    </div>\
    <label class="control-label" for="">Aktiv</label>\
    <div class="controls">\
        <select class="config-dropdown form-control" name="jtlshop3plugin-Install-Emailtemplate-Template-Active[]">\
            <option{if empty($tpl->Active) || $tpl->Active === 'Y'} selected {/if} value="Y">Ja</option>\
            <option{if !empty($tpl->Active) && $tpl->Active === 'N'} selected {/if} value="N">Nein</option>\
        </select>\
    </div>\
    <label class="control-label" for="">Anbieter-Kennzeichnung hinzuf&uuml;gen?</label>\
    <div class="controls">\
        <select class="config-dropdown form-control" name="jtlshop3plugin-Install-Emailtemplate-Template-AKZ[]">\
            <option{if isset($tpl->AKZ) && $tpl->AKZ === 1} selected {/if} value="1">Ja</option>\
            <option{if !isset($tpl->AKZ) || $tpl->AKZ === 0} selected {/if} value="0">Nein</option>\
        </select>\
    </div>\
    <label class="control-label" for="">AGB hinzuf&uuml;gen?</label>\
    <div class="controls">\
        <select class="config-dropdown form-control" name="jtlshop3plugin-Install-Emailtemplate-Template-AGB[]">\
            <option{if isset($tpl->AGB) && $tpl->AGB === 1} selected {/if} value="1">Ja</option>\
            <option{if !isset($tpl->AGB) || $tpl->AGB === 0} selected {/if} value="0">Nein</option>\
        </select>\
    </div>\
    <label class="control-label" for="">WRB hinzuf&uuml;gen?</label>\
    <div class="controls">\
        <select class="config-dropdown form-control" name="jtlshop3plugin-Install-Emailtemplate-Template-WRB[]">\
            <option{if isset($tpl->WRB) && $tpl->WRB === 1} selected {/if} value="1">Ja</option>\
            <option{if !isset($tpl->WRB) || $tpl->WRB === 0} selected {/if} value="0">Nein</option>\
        </select>\
    </div>\
    <label class="control-label" for="">Muster-Widerrufsformular hinzuf&uuml;gen?</label>\
    <div class="controls">\
        <select class="config-dropdown form-control" name="jtlshop3plugin-Install-Emailtemplate-Template-nWRBForm[]">\
            <option{if isset($tpl->nWRBForm) && $tpl->nWRBForm === 1} selected {/if} value="1">Ja</option>\
            <option{if !isset($tpl->nWRBForm) || $tpl->nWRBForm === 0} selected {/if} value="0">Nein</option>\
        </select>\
    </div>\
    <label class="control-label" for="">Betreff</label>\
    <div class="controls">\
        <input{if !empty($tpl->TemplateLanguage[0]->Subject)} value="{$tpl->TemplateLanguage[0]->Subject}" {/if} type="text" name="jtlshop3plugin-Install-Emailtemplate-Template-TemplateLanguage-Subject[]" class="form-control required" placeholder="Mein Betreff">\
    </div>\
    <label class="control-label" for="">Inhalt (HTML)</label>\
    <div class="controls">\
        <textarea name="jtlshop3plugin-Install-Emailtemplate-Template-TemplateLanguage-ContentHtml[]" class="form-control required">{if !empty($tpl->TemplateLanguage[0]->ContentHtml)}{$tpl->TemplateLanguage[0]->ContentHtml}{/if}</textarea>\
    </div>\
    <label class="control-label" for="">Inhalt (Text)</label>\
    <div class="controls">\
        <textarea name="jtlshop3plugin-Install-Emailtemplate-Template-TemplateLanguage-ContentText[]" class="form-control required">{if !empty($tpl->TemplateLanguage[0]->ContentText)}{$tpl->TemplateLanguage[0]->ContentText}{/if}</textarea>\
    </div>\
    <hr>\
</div>\
