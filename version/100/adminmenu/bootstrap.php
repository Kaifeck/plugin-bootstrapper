<?php
/**
 * @global Plugin    $oPlugin
 * @global JTLSmarty $smarty
 */

require_once $oPlugin->cAdminmenuPfad . 'inc/class.pluginBootstrapper.php';
$bootStrapper = new pluginBootstrapper(false);
$loadedPlugin = false;

if (!empty($_POST) && validateToken()) {
    if (isset($_POST['createPlugin'])) {
        $status       = $bootStrapper->bootstrap($_POST)->create()->getResponse();
        header('Content-Type: application/json');
        die(json_encode(['success' => count($status['errors']) === 0, 'status' => $status]));
    }
    if (isset($_POST['loadPlugin'])) {
        $path       = rtrim($_POST['path'], '/') . '/';
        $pluginPath = realpath(PFAD_ROOT . PFAD_PLUGIN . $path);
        if ($pluginPath === false ||
            !is_dir($pluginPath) ||
            strpos($pluginPath, PFAD_ROOT . PFAD_PLUGIN) === false ||
            !file_exists($pluginPath . '/info.xml')
        ) {
            $cFehler = 'Ung&uuml;ltiger Pfad.';
        } else {
            $loadedPlugin = $bootStrapper->loadPlugin($pluginPath . '/info.xml');
        }
    }
}

$smarty->assign('permissionOK', is_writable(PFAD_ROOT . PFAD_PLUGIN))
       ->assign('adminURL', Shop::getURL() . '/' . PFAD_ADMIN . 'plugin.php?kPlugin=' . $oPlugin->kPlugin)
       ->assign('hookList', $bootStrapper->getHookList())
       ->assign('tplPath', $oPlugin->cAdminmenuPfad . 'tpl/')
       ->assign('loadedPlugin', $loadedPlugin)
       ->display($oPlugin->cAdminmenuPfad . 'tpl/base.tpl');
