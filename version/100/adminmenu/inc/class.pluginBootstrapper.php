<?php

/**
 * Class pluginBootstrapper
 */
class pluginBootstrapper
{

    /**
     * @var array
     */
    private $postData = [];

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $author;

    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $flushTags;

    /**
     * @var string
     */
    private $pluginID;

    /**
     * @var int
     */
    private $xmlVersion = 100;

    /**
     * @var string
     */
    private $shop3Version;

    /**
     * @var string
     */
    private $shop4Version;

    /**
     * @var string
     */
    private $icon;

    /**
     * @var int
     */
    private $pluginVersion = 100;

    /**
     * @var string
     */
    private $pluginDir;

    /**
     * @var string
     */
    private $infoXML;

    /**
     * @var bool
     */
    private $hasCss = false;

    /**
     * @var bool
     */
    private $hasJs = false;

    /**
     * @var bool
     */
    private $hasSQL = false;

    /**
     * @var array
     */
    private $jsFiles = [];

    /**
     * @var array
     */
    private $cssFiles = [];

    /**
     * @var bool
     */
    private $hasHooks = false;

    /**
     * @var bool
     */
    private $hasFrontendLinks = false;

    /**
     * @var bool
     */
    private $hasLangVars = false;

    /**
     * @var bool
     */
    private $hasSettings = false;

    /**
     * @var bool
     */
    private $hasMailTemplates = false;

    /**
     * @var bool
     */
    private $hasBoxes = false;

    /**
     * @var bool
     */
    private $hasCustomLinks = false;

    /**
     * @var array
     */
    private $customLinks = [];

    /**
     * @var array
     */
    private $hooks = [];

    /**
     * @var array
     */
    private $boxes = [];

    /**
     * @var array
     */
    private $langVars = [];

    /**
     * @var array
     */
    private $settings = [];

    /**
     * @var array
     */
    private $frontendLinks = [];

    /**
     * @var array
     */
    private $mailTemplates = [];

    /**
     * @var array
     */
    private $errors = [];

    /**
     * @var bool
     */
    private $debug;

    /**
     * pluginBootstrapper constructor.
     *
     * @param bool  $debug
     */
    public function __construct($debug = false)
    {
        $this->debug = $debug;
    }

    /**
     * load existing plugin via path to info.xml
     *
     * @param string $xmlPath
     * @return bool|object
     */
    public function loadPlugin($xmlPath)
    {
        if (file_exists($xmlPath)) {
            $xml                    = simplexml_load_file($xmlPath);
            $plugin                 = new stdClass();
            $plugin->Name           = (string)$xml->Name;
            $plugin->Description    = (string)$xml->Description;
            $plugin->Author         = (string)$xml->Author;
            $plugin->URL            = (string)$xml->URL;
            $plugin->ShopVersion    = (string)$xml->ShopVersion;
            $plugin->Shop4Version   = (string)$xml->Shop4Version;
            $plugin->Icon           = (string)$xml->Icon;
            $plugin->PluginID       = (string)$xml->PluginID;
            $plugin->FlushTags      = (string)$xml->Install->FlushTags;
            $plugin->Settings       = [];
            $plugin->Customlinks    = [];
            $plugin->Frontendlinks  = [];
            $plugin->Boxes          = [];
            $plugin->Install        = new stdClass();
            $plugin->Emailtemplates = [];
            $plugin->SQL            = null;

            if (!empty($xml->Install->Hooks->Hook)) {
                $plugin->Hooks = [];
                foreach ($xml->Install->Hooks->Hook as $hook) {
                    $file = (string)$hook;
                    foreach ($hook->attributes() as $attribute) {
                        $hookData        = new stdClass();
                        $hookData->file  = $file;
                        $hookData->id    = (string)$attribute;
                        $plugin->Hooks[] = $hookData;
                        break;
                    }
                }
            }
            //just the first version for now
            if (!empty($xml->Install->Version->SQL)) {
                $plugin->SQL = (string)$xml->Install->Version->SQL;
            }
            if (!empty($xml->Install->Locales->Variable)) {
                $plugin->LangVars = [];
                foreach ($xml->Install->Locales->Variable as $langVar) {
                    $lv              = new stdClass();
                    $lv->Description = (string)$langVar->Description;
                    $lv->Name        = (string)$langVar->Name;
                    $lv->Values      = [];
                    foreach ($langVar->VariableLocalized as $localization) {
                        $lv->Values[(string)$localization->attributes()] = (string)$localization;
                    }
                    $plugin->LangVars[] = $lv;
                }
            }
            if (!empty($xml->Install->Adminmenu)) {
                if (!empty($xml->Install->Adminmenu->Customlink)) {
                    if (!is_array($xml->Install->Adminmenu->Customlink)) {
                        $arr = [$xml->Install->Adminmenu->Customlink];
                    } else {
                        $arr = $xml->Install->Adminmenu->Customlink;
                    }
                    foreach ($arr as $customLink) {
                        $cl                    = new stdClass();
                        $cl->Name              = (string)$customLink->Name;
                        $cl->Filename          = (string)$customLink->Filename;
                        $cl->Sort              = (int)$customLink->attributes();
                        $plugin->CustomLinks[] = $cl;
                    }
                }

                if (!empty($xml->Install->Adminmenu->Settingslink)) {
                    $sl           = new stdClass();
                    $sl->Sort     = (int)$xml->Install->Adminmenu->Settingslink->attributes();
                    $sl->Settings = [];
                    foreach ($xml->Install->Adminmenu->Settingslink->Setting as $_setting) {
                        $setting              = new stdClass();
                        $setting->Name        = (string)$_setting->Name;
                        $setting->ValueName   = (string)$_setting->ValueName;
                        $setting->Description = (string)$_setting->Description;
                        foreach ($_setting->attributes() as $attribute => $value) {
                            $setting->$attribute = (string)$value;
                        }
                        $sl->Settings[] = $setting;
                    }
                    $plugin->Settings[] = $sl;
                }
            }
            if (!empty($xml->Install->FrontendLink->Link)) {
                foreach ($xml->Install->FrontendLink->Link as $_frontendLink) {
                    $fl                     = new stdClass();
                    $fl->Name               = (string)$_frontendLink->Name;
                    $fl->Filename           = (string)$_frontendLink->Filename;
                    $fl->Template           = (string)$_frontendLink->Template;
                    $fl->FullscreenTemplate = (string)$_frontendLink->FullscreenTemplate;
                    $fl->VisibleAfterLogin  = (string)$_frontendLink->VisibleAfterLogin;
                    $fl->PrintButton        = (string)$_frontendLink->PrintButton;
                    $fl->SSL                = (int)$_frontendLink->SSL;
                    $fl->LinkGroup          = (string)$_frontendLink->LinkGroup;

                    if (isset($_frontendLink->LinkLanguage)) {
                        $fl->LinkLanguage = [];
                        foreach ($_frontendLink->LinkLanguage as $_linkLanguage) {
                            $ll                  = new stdClass();
                            $ll->Seo             = (string)$_linkLanguage->Seo;
                            $ll->Name            = (string)$_linkLanguage->Name;
                            $ll->Title           = (string)$_linkLanguage->Title;
                            $ll->MetaTitle       = (string)$_linkLanguage->MetaTitle;
                            $ll->MetaKeywords    = (string)$_linkLanguage->MetaKeywords;
                            $ll->MetaDescription = (string)$_linkLanguage->MetaDescription;

                            foreach ($_linkLanguage->attributes() as $k => $v) {
                                $ll->$k = (string)$v;
                            }
                            $fl->LinkLanguage[] = $ll;
                        }
                    }
                    $plugin->Frontendlinks[] = $fl;
                }
            }
            if (!empty($xml->Install->Emailtemplate->Template)) {
                foreach ($xml->Install->Emailtemplate->Template as $_template) {
                    $tpl              = new stdClass();
                    $tpl->Name        = (string)$_template->Name;
                    $tpl->Description = (string)$_template->Description;
                    $tpl->Type        = (string)$_template->Type;
                    $tpl->ModulId     = (string)$_template->ModulId;
                    $tpl->Active      = (string)$_template->Active;
                    $tpl->AKZ         = (int)$_template->AKZ;
                    $tpl->AGB         = (int)$_template->AGB;
                    $tpl->WRB         = (int)$_template->WRB;
                    $tpl->nWRBForm    = (int)$_template->nWRBForm;

                    if (isset($_template->TemplateLanguage)) {
                        $tpl->TemplateLanguage = [];
                        foreach ($_template->TemplateLanguage as $_tplLanguage) {
                            $templateLanguage              = new stdClass();
                            $templateLanguage->Subject     = (string)$_tplLanguage->Subject;
                            $templateLanguage->ContentHtml = (string)$_tplLanguage->ContentHtml;
                            $templateLanguage->ContentText = (string)$_tplLanguage->ContentText;

                            foreach ($_tplLanguage->attributes() as $k => $v) {
                                $templateLanguage->$k = (string)$v;
                            }
                            $tpl->TemplateLanguage[] = $templateLanguage;
                        }
                    }
                    $plugin->Emailtemplates[] = $tpl;
                }
            }

            if (!empty($xml->Install->Boxes->Box)) {
                foreach ($xml->Install->Boxes->Box as $_box) {
                    $box               = new stdClass();
                    $box->Available    = (int)$_box->Available;
                    $box->Name         = (string)$_box->Name;
                    $box->TemplateFile = (string)$_box->TemplateFile;
                    $plugin->Boxes[]   = $box;
                }
            }

            $plugin->Install->JS  = (isset($xml->Install->JS))
                ? $xml->Install->JS
                : null;
            $plugin->Install->CSS = (isset($xml->Install->CSS))
                ? $xml->Install->CSS
                : null;

            return $plugin;
        }

        return false;
    }

    /**
     * @param array $postData
     * @return $this
     */
    public function bootstrap($postData)
    {
        if ($this->debug === true) {
            Shop::dbg($postData, false, 'POST');
        }
        $this->postData = $postData;

        return $this->parseData();
    }

    /**
     * create the plugin
     *
     * @return $this
     */
    public function create()
    {
        if ($this->validateBaseInfo()) {
            $this->createDirsAndFiles()
                 ->writeXML();
        }

        return $this;
    }

    /**
     * @return bool
     */
    private function validateBaseInfo()
    {
        $ok = true;
        if (empty($this->name)) {
            $this->errors[] = 'Plugin-Name darf nicht leer sein.';
            $ok             = false;
        }
        if (empty($this->description)) {
            $this->errors[] = 'Plugin-Beschreibung darf nicht leer sein.';
            $ok             = false;
        }
        if (empty($this->author)) {
            $this->errors[] = 'Plugin-Autor darf nicht leer sein.';
            $ok             = false;
        }
        if (empty($this->url)) {
            $this->errors[] = 'Plugin-Homepage darf nicht leer sein.';
            $ok             = false;
        }
        if (empty($this->pluginID)) {
            $this->errors[] = 'Plugin-ID darf nicht leer sein.';
            $ok             = false;
        }
        if (empty($this->pluginDir)) {
            $this->errors[] = 'Pluginordner konnte nicht generiert werden.';
            $ok             = false;
        }
        if (empty($this->shop4Version) && empty($this->shop3Version)) {
            $this->errors[] = 'Shop 3 und/oder 4 muss unterst&uuml;tzt werden.';
            $ok             = false;
        }

        return $ok;
    }

    /**
     * prepare array of more or less important information as an ajax response
     *
     * @return array
     */
    public function getResponse()
    {
        return [
            'files'     => $this->getFileList(),
            'dirs'      => $this->getDirList(),
            'plugin'    => $this->pluginID,
            'plugindir' => $this->pluginDir,
            'errors'    => $this->errors,
            'POST'      => $this->postData
        ];
    }

    /**
     * create all directories and files for the new plugin
     *
     * @return $this
     */
    private function createDirsAndFiles()
    {
        $res      = true;
        $dirList  = $this->getDirList();
        $fileList = $this->getFileList();

        if ($this->debug === true) {
            Shop::dbg($dirList, false, 'dirlist');
            Shop::dbg($fileList, false, 'filelist');
        }

        foreach ($dirList as $_dir) {
            if (!is_dir($_dir)) {
                $res = $res && mkdir($_dir);
                if ($res === false) {
                    $this->errors[] = 'Konnte Ordner nicht erstellen: ' . $_dir;
                    break;
                }
            }
        }
        foreach ($fileList as $_file) {
            if (!file_exists($_file['file'])) {
                if ($_file['type'] === 'php') {
                    $content = "<?php \n";
                    if (!empty($_file['hookName'])) {
                        $content .= "/**\n* Hook " . $_file['hookName'] .
                            "\n*\n* @global JTLSmarty \$smarty\n* @global Plugin \$oPlugin\n*/";
                    }
                    $res = $res && (file_put_contents($_file['file'], $content) > 0);
                } else {
                    $res = $res && (file_put_contents($_file['file'], '') === 0);
                }
                if ($res === false) {
                    $this->errors[] = 'Konnte Datei nicht erstellen: ' . $_file['file'];
                    break;
                }
            }
        }

        return $this;
    }

    /**
     * parse POST data
     *
     * @return $this
     */
    private function parseData()
    {
        $this->name         = $this->postData['jtlshop3plugin-Name'];
        $this->description  = $this->postData['jtlshop3plugin-Description'];
        $this->author       = $this->postData['jtlshop3plugin-Author'];
        $this->url          = $this->postData['jtlshop3plugin-URL'];
        $this->pluginID     = $this->postData['jtlshop3plugin-PluginID'];
        $this->flushTags    = (!empty($this->postData['jtlshop3plugin-FlushTags']))
            ? $this->postData['jtlshop3plugin-FlushTags']
            : null;
        $this->shop3Version = (isset($this->postData['supportsShop3']) &&
            $this->postData['supportsShop3'] === 'on' &&
            !empty($this->postData['jtlshop3plugin-ShopVersion']))
            ? $this->postData['jtlshop3plugin-ShopVersion']
            : null;
        $this->shop4Version = (isset($this->postData['supportsShop4']) &&
            $this->postData['supportsShop4'] === 'on' &&
            !empty($this->postData['jtlshop3plugin-Shop4Version']))
            ? $this->postData['jtlshop3plugin-Shop4Version']
            : null;
        $this->icon         = (!empty($this->postData['jtlshop3plugin-Icon']))
            ? $this->postData['jtlshop3plugin-Icon']
            : null;
        $this->hasSQL       = (!empty($this->postData['jtlshop3plugin-Install-Version-SQL']) &&
            $this->postData['jtlshop3plugin-Install-Version-SQL'] === 'on');

        if (isset($this->postData['jtlshop3plugin-Install-CSS-file-name']) &&
            isset($this->postData['jtlshop3plugin-Install-CSS-file-priority']) &&
            is_array($this->postData['jtlshop3plugin-Install-CSS-file-name']) &&
            is_array($this->postData['jtlshop3plugin-Install-CSS-file-priority']) &&
            count($this->postData['jtlshop3plugin-Install-CSS-file-name']) ===
            count($this->postData['jtlshop3plugin-Install-CSS-file-priority'])
        ) {
            foreach ($this->postData['jtlshop3plugin-Install-CSS-file-name'] as $idx => $file) {
                $this->cssFiles[] = [
                    'name'     => $file,
                    'priority' => $this->postData['jtlshop3plugin-Install-CSS-file-priority'][$idx]
                ];
            }
        }
        if (isset($this->postData['jtlshop3plugin-Install-JS-file-name']) &&
            isset($this->postData['jtlshop3plugin-Install-JS-file-priority']) &&
            is_array($this->postData['jtlshop3plugin-Install-JS-file-name']) &&
            is_array($this->postData['jtlshop3plugin-Install-JS-file-priority']) &&
            count($this->postData['jtlshop3plugin-Install-JS-file-name']) ===
            count($this->postData['jtlshop3plugin-Install-JS-file-priority'])
        ) {
            foreach ($this->postData['jtlshop3plugin-Install-JS-file-name'] as $idx => $file) {
                $this->jsFiles[] = [
                    'name'     => $file,
                    'priority' => $this->postData['jtlshop3plugin-Install-JS-file-priority'][$idx],
                    'position' => $this->postData['jtlshop3plugin-Install-JS-file-position'][$idx]
                ];
            }
        }

        if (isset($this->postData['jtlshop3plugin-Install-Hooks-Hook']) &&
            isset($this->postData['jtlshop3plugin-Install-Hooks-Hook-ID']) &&
            count($this->postData['jtlshop3plugin-Install-Hooks-Hook-ID']) ===
            count($this->postData['jtlshop3plugin-Install-Hooks-Hook'])
        ) {
            foreach ($this->postData['jtlshop3plugin-Install-Hooks-Hook'] as $idx => $hookFile) {
                $this->hooks[] = [
                    'file' => $hookFile,
                    'id'   => $this->postData['jtlshop3plugin-Install-Hooks-Hook-ID'][$idx]
                ];
            }
        }

        if (!empty($this->postData['jtlshop3plugin-Install-Boxes-Box-Name']) &&
            !empty($this->postData['jtlshop3plugin-Install-Boxes-Box-TemplateFile']) &&
            count($this->postData['jtlshop3plugin-Install-Boxes-Box-Name']) ===
            count($this->postData['jtlshop3plugin-Install-Boxes-Box-TemplateFile'])
        ) {
            foreach ($this->postData['jtlshop3plugin-Install-Boxes-Box-Name'] as $idx => $_box) {
                $this->boxes[] = [
                    'Name'         => $_box,
                    'Available'    => $this->postData['jtlshop3plugin-Install-Boxes-Box-Available'][$idx],
                    'TemplateFile' => $this->postData['jtlshop3plugin-Install-Boxes-Box-TemplateFile'][$idx]
                ];
            }
        }

        if (!empty($this->postData['jtlshop3plugin-Locales-Variable-Name']) &&
            isset($this->postData['jtlshop3plugin-Locales-Variable-Description']) &&
            isset($this->postData['jtlshop3plugin-Locales-Variable_GER']) &&
            isset($this->postData['jtlshop3plugin-Locales-Variable_ENG']) &&
            count($this->postData['jtlshop3plugin-Locales-Variable-Name']) ===
            count($this->postData['jtlshop3plugin-Locales-Variable-Description'])
        ) {
            foreach ($this->postData['jtlshop3plugin-Locales-Variable-Name'] as $idx => $_langVar) {
                $this->langVars[] = [
                    'Name'                  => $_langVar,
                    'Description'           => $this->postData['jtlshop3plugin-Locales-Variable-Description'][$idx],
                    'VariableLocalized_ENG' => $this->postData['jtlshop3plugin-Locales-Variable_ENG'][$idx],
                    'VariableLocalized_GER' => $this->postData['jtlshop3plugin-Locales-Variable_GER'][$idx]
                ];
            }
        }

        if (!empty($this->postData['jtlshop3plugin-Settingslink-Setting-Name']) &&
            !empty($this->postData['jtlshop3plugin-Settingslink-Setting-ValueName']) &&
            !empty($this->postData['jtlshop3plugin-Settingslink-Setting-type']) &&
            count($this->postData['jtlshop3plugin-Settingslink-Setting-Name']) ===
            count($this->postData['jtlshop3plugin-Settingslink-Setting-ValueName']) &&
            count($this->postData['jtlshop3plugin-Settingslink-Setting-type']) ===
            count($this->postData['jtlshop3plugin-Settingslink-Setting-ValueName'])
        ) {
            foreach ($this->postData['jtlshop3plugin-Settingslink-Setting-Name'] as $idx => $_setting) {
                $setting = [
                    'Name'         => $_setting,
                    'ValueName'    => $this->postData['jtlshop3plugin-Settingslink-Setting-ValueName'][$idx],
                    'type'         => $this->postData['jtlshop3plugin-Settingslink-Setting-type'][$idx],
                    'Description'  => $this->postData['jtlshop3plugin-Settingslink-Setting-Description'][$idx],
                    'initialValue' => $this->postData['jtlshop3plugin-Settingslink-Setting-initialValue'][$idx],
                    'sort'         => 1,//@todo
                    'conf'         => 'Y'//@todo
                ];

                if ($setting['type'] === 'radio' || $setting['type'] === 'selectbox') {
                    for ($i = 0; $i < 100; $i++) {
                        if (isset($this->postData['jtlshop3plugin-Install-Adminmenu-Settingslink-Setting-Options-Option-' . $i])) {
                            $setting['options'] = [];
                            foreach ($this->postData['jtlshop3plugin-Install-Adminmenu-Settingslink-Setting-Options-Option-' . $i] as $optIdx => $_option) {
                                $setting['options'][] = [
                                    'value'  => $this->postData['jtlshop3plugin-Install-Adminmenu-Settingslink-Setting-Options-Option-value-' . $i][$optIdx],
                                    'sort'   => $optIdx,
                                    'option' => $_option
                                ];
                            }

                            unset($this->postData['jtlshop3plugin-Install-Adminmenu-Settingslink-Setting-Options-Option-' . $i]);
                            break;
                        }
                    }
                }

                $this->settings[] = $setting;
            }
        }

        if (!empty($this->postData['jtlshop3plugin-FrontendLink-Link-Name']) &&
            !empty($this->postData['jtlshop3plugin-FrontendLink-Link-Filename']) &&
            (count($this->postData['jtlshop3plugin-FrontendLink-Link-Name']) ===
                count($this->postData['jtlshop3plugin-FrontendLink-Link-Filename']))
        ) {
            foreach ($this->postData['jtlshop3plugin-FrontendLink-Link-Name'] as $idx => $frontendLink) {
                $this->frontendLinks[] = [
                    'Filename'                     => $this->postData['jtlshop3plugin-FrontendLink-Link-Filename'][$idx],
                    'Name'                         => $this->postData['jtlshop3plugin-FrontendLink-Link-Name'][$idx],
                    'Template'                     => $this->postData['jtlshop3plugin-FrontendLink-Link-Template'][$idx],
                    'FullscreenTemplate'           => $this->postData['jtlshop3plugin-FrontendLink-Link-FullscreenTemplate'][$idx],
                    'VisibleAfterLogin'            => $this->postData['jtlshop3plugin-FrontendLink-Link-VisibleAfterLogin'][$idx],
                    'PrintButton'                  => $this->postData['jtlshop3plugin-FrontendLink-Link-PrintButton'][$idx],
                    'SSL'                          => $this->postData['jtlshop3plugin-FrontendLink-Link-Filename'][$idx],
                    'LinkGroup'                    => $this->postData['jtlshop3plugin-FrontendLink-Link-LinkGroup'][$idx],
                    'LinkLanguage_Seo'             => $this->postData['jtlshop3plugin-FrontendLink-Link-LinkLanguage-Seo'][$idx],
                    'LinkLanguage_Name'            => $this->postData['jtlshop3plugin-FrontendLink-Link-LinkLanguage-Name'][$idx],
                    'LinkLanguage_Title'           => $this->postData['jtlshop3plugin-FrontendLink-Link-LinkLanguage-Title'][$idx],
                    'LinkLanguage_MetaTitle'       => $this->postData['jtlshop3plugin-FrontendLink-Link-LinkLanguage-MetaTitle'][$idx],
                    'LinkLanguage_MetaKeywords'    => $this->postData['jtlshop3plugin-FrontendLink-Link-LinkLanguage-MetaKeywords'][$idx],
                    'LinkLanguage_MetaDescription' => $this->postData['jtlshop3plugin-FrontendLink-Link-LinkLanguage-MetaDescription'][$idx],
                ];
            }
        }

        if (!empty($this->postData['jtlshop3plugin-Install-Emailtemplate-Template-Name'])) {
            foreach ($this->postData['jtlshop3plugin-Install-Emailtemplate-Template-Name'] as $idx => $_mailTemplate) {
                $this->mailTemplates[] = [
                    'Name'                         => $_mailTemplate,
                    'Description'                  => $this->postData['jtlshop3plugin-Install-Emailtemplate-Template-Description'][$idx],
                    'Type'                         => $this->postData['jtlshop3plugin-Install-Emailtemplate-Template-Type'][$idx],
                    'ModulId'                      => $this->postData['jtlshop3plugin-Install-Emailtemplate-Template-ModulId'][$idx],
                    'Active'                       => $this->postData['jtlshop3plugin-Install-Emailtemplate-Template-Active'][$idx],
                    'AKZ'                          => $this->postData['jtlshop3plugin-Install-Emailtemplate-Template-AKZ'][$idx],
                    'AGB'                          => $this->postData['jtlshop3plugin-Install-Emailtemplate-Template-AGB'][$idx],
                    'WRB'                          => $this->postData['jtlshop3plugin-Install-Emailtemplate-Template-WRB'][$idx],
                    'nWRBForm'                     => $this->postData['jtlshop3plugin-Install-Emailtemplate-Template-nWRBForm'][$idx],
                    'TemplateLanguage_Subject'     => $this->postData['jtlshop3plugin-Install-Emailtemplate-Template-TemplateLanguage-Subject'][$idx],
                    'TemplateLanguage_ContentHtml' => $this->postData['jtlshop3plugin-Install-Emailtemplate-Template-TemplateLanguage-ContentHtml'][$idx],
                    'TemplateLanguage_ContentText' => $this->postData['jtlshop3plugin-Install-Emailtemplate-Template-TemplateLanguage-ContentText'][$idx]
                ];
            }
        }
        if (!empty($this->postData['jtlshop3plugin-Install-Adminmenu-Customlink-Name']) && !empty($this->postData['jtlshop3plugin-Install-Adminmenu-Customlink-Filename']) &&
            count($this->postData['jtlshop3plugin-Install-Adminmenu-Customlink-Name']) === count($this->postData['jtlshop3plugin-Install-Adminmenu-Customlink-Filename'])
        ) {
            foreach ($this->postData['jtlshop3plugin-Install-Adminmenu-Customlink-Name'] as $idx => $_customLink) {
                $this->customLinks[] = [
                    'Name'     => $_customLink,
                    'Filename' => $this->postData['jtlshop3plugin-Install-Adminmenu-Customlink-Filename'][$idx],
                ];
            }
        }

        $this->hasHooks         = count($this->hooks) > 0;
        $this->hasJs            = count($this->jsFiles) > 0;
        $this->hasCss           = count($this->cssFiles) > 0;
        $this->hasBoxes         = count($this->boxes) > 0;
        $this->hasLangVars      = count($this->langVars) > 0;
        $this->hasSettings      = count($this->settings) > 0;
        $this->hasFrontendLinks = count($this->frontendLinks) > 0;
        $this->hasMailTemplates = count($this->mailTemplates) > 0;
        $this->hasCustomLinks   = count($this->customLinks) > 0;

        $this->pluginDir = PFAD_ROOT . PFAD_PLUGIN . $this->pluginID;

        return $this;
    }

    /**
     * creates the info.xml xml tree
     *
     * @return string
     */
    private function getXML()
    {
        $head = '<?xml version="1.0" encoding="ISO-8859-1"?>' . "\n";
        $head .= '<jtlshop3plugin>' . "\n";

        $head .= "\t" . '<Name>' . $this->name . '</Name>' . "\n";
        $head .= "\t" . '<Description>' . $this->description . '</Description>' . "\n";
        $head .= "\t" . '<Author>' . $this->author . '</Author>' . "\n";
        $head .= "\t" . '<URL>' . $this->url . '</URL>' . "\n";
        $head .= "\t" . '<PluginID>' . $this->pluginID . '</PluginID>' . "\n";
        $head .= "\t" . '<XMLVersion>' . $this->xmlVersion . '</XMLVersion>' . "\n";
        if ($this->shop3Version !== null) {
            $head .= "\t" . '<ShopVersion>' . $this->shop3Version . '</ShopVersion>' . "\n";
        }
        if ($this->shop4Version !== null) {
            $head .= "\t" . '<Shop4Version>' . $this->shop4Version . '</Shop4Version>' . "\n";
        }
        if ($this->icon !== null) {
            $head .= "\t" . '<Icon>' . $this->postData['jtlshop3plugin-Icon'] . '</Icon>' . "\n";
        }
        $body = "\t" . '<Install>' . "\n";

        $body .= "\t\t" . '<Version nr="100">' . "\n";
        $dt = new DateTime();
        $body .= "\t\t\t" . '<CreateDate>' . $dt->format('Y-m-d') . '</CreateDate>' . "\n";

        if ($this->hasSQL) {
            //@todo: make configurable?
            $body .= "\t\t\t" . '<SQL>install-100.sql</SQL>' . "\n";
        }
        $body .= "\t\t" . '</Version>' . "\n";
        if ($this->flushTags !== null) {
            $body .= "\t\t" . '<FlushTags>' . $this->flushTags . "</FlushTags>\n";
        }
        if ($this->hasHooks) {
            $body .= "\t\t" . "<Hooks>\n";
            foreach ($this->hooks as $_hook) {
                $body .= "\t\t\t" . '<Hook id="' . $_hook['id'] . '">' . $_hook['file'] . '</Hook>' . "\n";
            }
            $body .= "\t\t" . "</Hooks>\n";
        }

        if ($this->hasSettings || $this->hasCustomLinks) {
            $body .= "\t\t" . "<Adminmenu>\n";
            if ($this->hasSettings) {
                $body .= "\t\t\t" . '<Settingslink sort="2">' . "\n"; //@todo: sort
                $body .= "\t\t\t\t" . '<Name>Einstellungen</Name>' . "\n"; //@todo: configurable?
                foreach ($this->settings as $_setting) {
                    $body .= "\t\t\t\t" . '<Setting type="' . $_setting['type'] . '" initialValue="' .
                        $_setting['initialValue'] . '" sort="' . $_setting['sort'] .
                        '" conf="' . $_setting['conf'] . '">' . "\n";
                    $body .= "\t\t\t\t\t" . '<Name>' . $_setting['Name'] . '</Name>' . "\n";
                    $body .= "\t\t\t\t\t" . '<Description>' . $_setting['Description'] . '</Description>' . "\n";
                    $body .= "\t\t\t\t\t" . '<ValueName>' . $_setting['ValueName'] . '</ValueName>' . "\n";

                    if (isset($_setting['options']) &&
                        ($_setting['type'] === 'radio' || $_setting['type'] === 'selectbox')
                    ) {
                        if ($_setting['type'] === 'selectbox') {
                            $body .= "\t\t\t\t\t" . '<SelectboxOptions>' . "\n";
                        } else {
                            $body .= "\t\t\t\t\t" . '<RadioOptions>' . "\n";
                        }

                        foreach ($_setting['options'] as $_opt) {
                            $body .= "\t\t\t\t\t\t" . '<Option value="' . $_opt['value'] .
                                '" sort="' . $_opt['sort'] . '">' . $_opt['option'] . '</Option>' . "\n";
                        }

                        if ($_setting['type'] === 'selectbox') {
                            $body .= "\t\t\t\t\t" . '</SelectboxOptions>' . "\n";
                        } else {
                            $body .= "\t\t\t\t\t" . '</RadioOptions>' . "\n";
                        }
                    }

                    $body .= "\t\t\t\t" . '</Setting>' . "\n";
                }
                $body .= "\t\t\t" . "</Settingslink>\n";
            }

            if ($this->hasCustomLinks) {
                foreach ($this->customLinks as $_idx => $_customLink) {
                    $body .= "\t\t\t" . '<Customlink sort="' . ($_idx + 1) . '">' . "\n";
                    $body .= "\t\t\t\t" . '<Name>' . $_customLink['Name'] . '</Name>' . "\n";
                    $body .= "\t\t\t\t" . '<Filename>' . $_customLink['Filename'] . '</Filename>' . "\n";
                    $body .= "\t\t\t" . '</Customlink>' . "\n";
                }
            }

            $body .= "\t\t" . "</Adminmenu>\n";
        }

        if ($this->hasLangVars) {
            $body .= "\t\t" . "<Locales>\n";
            foreach ($this->langVars as $_lv) {
                $body .= "\t\t\t" . '<Variable>' . "\n";
                $body .= "\t\t\t\t" . '<Name>' . $_lv['Name'] . '</Name>' . "\n";
                $body .= "\t\t\t\t" . '<Description>' . $_lv['Description'] . '</Description>' . "\n";
                $body .= "\t\t\t\t" . '<VariableLocalized iso="GER">' .
                    $_lv['VariableLocalized_GER'] .
                    '</VariableLocalized>' . "\n";
                $body .= "\t\t\t\t" . '<VariableLocalized iso="ENG">' .
                    $_lv['VariableLocalized_ENG'] .
                    '</VariableLocalized>' . "\n";
                $body .= "\t\t\t" . '</Variable>' . "\n";
            }
            $body .= "\t\t" . "</Locales>\n";
        }

        if ($this->hasFrontendLinks) {
            $body .= "\t\t" . "<FrontendLink>\n";
            foreach ($this->frontendLinks as $_link) {
                $body .= "\t\t\t" . '<Link>' . "\n";
                $body .= "\t\t\t\t" . '<Filename>' . $_link['Filename'] . '</Filename>' . "\n";
                $body .= "\t\t\t\t" . '<Name>' . $_link['Name'] . '</Name>' . "\n";
                if (!empty($_link['FullscreenTemplate'])) {
                    $body .= "\t\t\t\t" . '<FullscreenTemplate>' .
                        $_link['FullscreenTemplate'] .
                        '</FullscreenTemplate>' . "\n";
                } elseif (!empty($_link['Template'])) {
                   $body .= "\t\t\t\t" . '<Template>' . $_link['Template'] . '</Template>' . "\n";
                }
                if (!empty($_link['LinkGroup'])) {
                    $body .= "\t\t\t\t" . '<LinkGroup>' . $_link['LinkGroup'] . '</LinkGroup>' . "\n";
                }
                $body .= "\t\t\t\t" . '<VisibleAfterLogin>' .
                    $_link['VisibleAfterLogin'] .
                    '</VisibleAfterLogin>' . "\n";
                $body .= "\t\t\t\t" . '<PrintButton>' . $_link['PrintButton'] . '</PrintButton>' . "\n";
                $body .= "\t\t\t\t" . '<SSL>' . $_link['SSL'] . '</SSL>' . "\n";

                $body .= "\t\t\t\t" . '<LinkLanguage iso="GER">' . "\n";
                $body .= "\t\t\t\t\t" . '<Seo>' . $_link['LinkLanguage_Seo'] . '</Seo>' . "\n";
                $body .= "\t\t\t\t\t" . '<Name>' . $_link['LinkLanguage_Name'] . '</Name>' . "\n";
                $body .= "\t\t\t\t\t" . '<Title>' . $_link['LinkLanguage_Title'] . '</Title>' . "\n";
                $body .= "\t\t\t\t\t" . '<MetaTitle>' . $_link['LinkLanguage_MetaTitle'] . '</MetaTitle>' . "\n";
                $body .= "\t\t\t\t\t" . '<MetaKeywords>' .
                    $_link['LinkLanguage_MetaKeywords'] .
                    '</MetaKeywords>' . "\n";
                $body .= "\t\t\t\t\t" . '<MetaDescription>' .
                    $_link['LinkLanguage_MetaDescription'] .
                    '</MetaDescription>' . "\n";
                $body .= "\t\t\t\t" . '</LinkLanguage>' . "\n";

                $body .= "\t\t\t" . '</Link>' . "\n";
            }
            $body .= "\t\t" . "</FrontendLink>\n";
        }

        if ($this->hasMailTemplates) {
            $body .= "\t\t" . "<Emailtemplate>\n";
            foreach ($this->mailTemplates as $_mailTpl) {
                $body .= "\t\t\t" . '<Template>' . "\n";
                $body .= "\t\t\t\t" . '<Name>' . $_mailTpl['Name'] . '</Name>' . "\n";
                $body .= "\t\t\t\t" . '<Description>' . $_mailTpl['Description'] . '</Description>' . "\n";
                $body .= "\t\t\t\t" . '<Type>' . $_mailTpl['Type'] . '</Type>' . "\n";
                $body .= "\t\t\t\t" . '<ModulId>' . $_mailTpl['ModulId'] . '</ModulId>' . "\n";
                $body .= "\t\t\t\t" . '<Active>' . $_mailTpl['Active'] . '</Active>' . "\n";
                $body .= "\t\t\t\t" . '<AKZ>' . $_mailTpl['AKZ'] . '</AKZ>' . "\n";
                $body .= "\t\t\t\t" . '<AGB>' . $_mailTpl['AGB'] . '</AGB>' . "\n";
                $body .= "\t\t\t\t" . '<WRB>' . $_mailTpl['WRB'] . '</WRB>' . "\n";
                $body .= "\t\t\t\t" . '<nWRBForm>' . $_mailTpl['nWRBForm'] . '</nWRBForm>' . "\n";
                $body .= "\t\t\t\t" . '<TemplateLanguage iso="GER">' . "\n";
                $body .= "\t\t\t\t\t" . '<Subject>' . $_mailTpl['TemplateLanguage_Subject'] . '</Subject>' . "\n";
                $body .= "\t\t\t\t\t" . '<ContentHtml>' .
                    $_mailTpl['TemplateLanguage_ContentHtml'] .
                    '</ContentHtml>' . "\n";
                $body .= "\t\t\t\t\t" . '<ContentText>' .
                    $_mailTpl['TemplateLanguage_ContentText'] .
                    '</ContentText>' . "\n";
                $body .= "\t\t\t\t" . '</TemplateLanguage>' . "\n";

                $body .= "\t\t\t" . '</Template>' . "\n";
            }
            $body .= "\t\t" . "</Emailtemplate>\n";
        }

        if ($this->hasBoxes) {
            $body .= "\t\t" . "<Boxes>\n";
            foreach ($this->boxes as $_box) {
                $body .= "\t\t\t" . '<Box>' . "\n";
                $body .= "\t\t\t\t" . '<Name>' . $_box['Name'] . '</Name>' . "\n";
                $body .= "\t\t\t\t" . '<Available>' . $_box['Available'] . '</Available>' . "\n";
                $body .= "\t\t\t\t" . '<TemplateFile>' . $_box['TemplateFile'] . '</TemplateFile>' . "\n";
                $body .= "\t\t\t" . '</Box>' . "\n";
            }
            $body .= "\t\t" . "</Boxes>\n";
        }

        if ($this->hasCss) {
            $body .= "\t\t" . "<CSS>\n";
            foreach ($this->cssFiles as $_css) {
                $body .= "\t\t\t" . '<file>' . "\n";
                $body .= "\t\t\t\t" . '<name>' . $_css['name'] . '</name>' . "\n";
                $body .= "\t\t\t\t" . '<priority>' . $_css['priority'] . '</priority>' . "\n";
                $body .= "\t\t\t" . '</file>' . "\n";
            }
            $body .= "\t\t" . "</CSS>\n";
        }

        if ($this->hasJs) {
            $body .= "\t\t" . "<JS>\n";
            foreach ($this->jsFiles as $_js) {
                $body .= "\t\t\t" . '<file>' . "\n";
                $body .= "\t\t\t\t" . '<name>' . $_js['name'] . '</name>' . "\n";
                $body .= "\t\t\t\t" . '<priority>' . $_js['priority'] . '</priority>' . "\n";
                $body .= "\t\t\t\t" . '<position>' . $_js['position'] . '</position>' . "\n";
                $body .= "\t\t\t" . '</file>' . "\n";
            }
            $body .= "\t\t" . "</JS>\n";
        }

        $body .= "\t" . '</Install>' . "\n";

        $footer = '</jtlshop3plugin>';

        if ($this->debug === true) {
            Shop::dbg($head . $body . $footer, false, 'XML:');
        }

        return $head . $body . $footer;
    }

    /**
     * generate list of dirs to be created
     *
     * @return array
     */
    private function getDirList()
    {
        $pluginDir = $this->pluginDir;

        $dirList   = [];
        $dirList[] = $pluginDir;
        $dirList[] = $pluginDir . '/version';
        $dirList[] = $pluginDir . '/version/' . $this->pluginVersion;
        if ($this->hasCss || $this->hasJs || $this->hasHooks || $this->hasFrontendLinks || $this->hasBoxes) {
            $dirList[] = $pluginDir . '/version/' . $this->pluginVersion . '/frontend';
            if ($this->hasCss) {
                $dirList[] = $pluginDir . '/version/' . $this->pluginVersion . '/frontend/css';
            }
            if ($this->hasJs) {
                $dirList[] = $pluginDir . '/version/' . $this->pluginVersion . '/frontend/js';
            }
            if ($this->hasBoxes) {
                $dirList[] = $pluginDir . '/version/' . $this->pluginVersion . '/frontend/boxen';
            }
            foreach ($this->frontendLinks as $_link) {
                if (!empty($_link['FullscreenTemplate']) || !empty($_link['Template'])) {
                    $dirList[] = $pluginDir . '/version/' . $this->pluginVersion . '/frontend/template';
                    break;
                }
            }
        }
        if ($this->hasSQL) {
            $dirList[] = $pluginDir . '/version/' . $this->pluginVersion . '/sql';
        }

        if ($this->hasCustomLinks) {
            $dirList[] = $pluginDir . '/version/' . $this->pluginVersion . '/adminmenu';
        }

        return $dirList;
    }

    /**
     * generate list of files to be created
     *
     * @return array
     */
    private function getFileList()
    {
        $baseDir       = $this->pluginDir . '/version/' . $this->pluginVersion . '/';
        $this->infoXML = $this->pluginDir . '/info.xml';
        $fileList      = [];

        $fileList[] = [
            'file' => $this->infoXML,
            'type' => 'xml'
        ];

        foreach ($this->hooks as $_hook) {
            $fileList[] = [
                'file'     => $baseDir . 'frontend/' . $_hook['file'],
                'type'     => 'php',
                'hook'     => $_hook['id'],
                'hookName' => $this->getHookNameByID($_hook['id'])
            ];
        }
        foreach ($this->boxes as $_box) {
            $fileList[] = [
                'file' => $baseDir . 'frontend/boxen/' . $_box['TemplateFile'],
                'type' => 'xml'
            ];
        }
        foreach ($this->jsFiles as $_jsFile) {
            $fileList[] = [
                'file' => $baseDir . 'frontend/js/' . $_jsFile['name'],
                'type' => 'tpl'
            ];
        }
        foreach ($this->cssFiles as $_cssFile) {
            $fileList[] = [
                'file' => $baseDir . 'frontend/css/' . $_cssFile['name'],
                'type' => 'css'
            ];
        }
        foreach ($this->frontendLinks as $_link) {
            $fileList[] = [
                'file' => $baseDir . 'frontend/' . $_link['Filename'],
                'type' => 'php'
            ];
            if (!empty($_link['FullscreenTemplate'])) {
                $fileList[] = [
                    'file' => $baseDir . 'frontend/template/' . $_link['FullscreenTemplate'],
                    'type' => 'tpl'
                ];
            } elseif (!empty($_link['Template'])) {
                $fileList[] = [
                    'file' => $baseDir . 'frontend/template/' . $_link['Template'],
                    'type' => 'tpl'
                ];
            }
        }
        foreach ($this->customLinks as $_customLink) {
            $fileList[] = [
                'file' => $baseDir . 'adminmenu/' . $_customLink['Filename'],
                'type' => 'php'
            ];
        }
        if ($this->hasSQL) {
            $fileList[] = [
                'file' => $baseDir . 'sql/install-100.sql',
                'type' => 'sql'
            ];
        }

        return $fileList;
    }

    /**
     * write config xml to info.xml
     *
     * @return bool
     */
    public function writeXML()
    {
        if ($this->debug === true) {
            Shop::dbg($this->infoXML, false, 'write to info.xml:');
        }

        return ($this->infoXML !== null) 
            ? file_put_contents($this->infoXML, $this->getXML()) > 0 
            : false;
    }

    /**
     * @param string|int $id
     * @return string
     */
    private function getHookNameByID($id)
    {
        foreach ($this->getHookList() as $hook) {
            if ($hook[2] == $id) {
                return $hook[1];
            }
        }

        return '';
    }

    /**
     * @return array
     */
    public function getHookList()
    {
        $hookText = file_get_contents(PFAD_ROOT . PFAD_INCLUDES . 'hooks_inc.php');
        preg_match_all("/define\('(.*)', (.*)\);/", $hookText, $hooks, PREG_SET_ORDER);

        return $hooks;
    }
}